console.log("hello")

type Currencies = "jpy" | "eur" | "cad";

let currencyConverter = (amount: number, currentCurrency: Currencies, convertedCurrency: Currencies): number => {
    if (currentCurrency === "jpy") {
        return convertedCurrency === "eur" ? amount * 0.0077 : amount * 0.015;
    } else if (currentCurrency === "eur") {
        return convertedCurrency === "cad" ? amount * 1.5 : amount * 130;
    } else {
        return convertedCurrency === "eur" ? amount * 0.67 : amount * 87;
    }
};

console.log(currencyConverter(16,"jpy", "cad"))
type ShipmentCountry = "japon" | "france" | "canada";

let shipmentPrices = (weight: number, dimensions_length: number, dimensions_height: number, dimensions_width: number, country: ShipmentCountry): string => {
    let baseRateEUR: number;
    let baseRateCAD: number;
    let baseRateJPY: number;

    if (weight <= 1) {
        baseRateEUR = 10;
        baseRateCAD = 15;
        baseRateJPY = 1000;
    } else if (weight <= 3) {
        baseRateEUR = 20;
        baseRateCAD = 30;
        baseRateJPY = 2000;
    } else {
        baseRateEUR = 30;
        baseRateCAD = 45;
        baseRateJPY = 3000;
    }

    const dimensionTotal = dimensions_length + dimensions_height + dimensions_width;
    const dimensionSurchargeEUR = dimensionTotal > 150 ? baseRateEUR + 5 : 0;
    const dimensionSurchargeCAD = dimensionTotal > 150 ? baseRateCAD + 7.5 : 0;
    const dimensionSurchargeJPY = dimensionTotal > 150 ? baseRateJPY + 500 : 0;

    const totalEUR = dimensionSurchargeCAD;
    const totalCAD = dimensionSurchargeCAD;
    const totalJPY = dimensionSurchargeJPY;

    const totalAmount = (amount: number, currency: Currencies): string => {
        return `${amount} ${currency}`;
    };

    if (country === "france") {
        return `Les frais de livraison pour un colis de ${weight} kg sont de ${totalAmount(dimensionSurchargeEUR, "eur")}.`;
    } else if (country === "canada") {
        return `Les frais de livraison pour un colis de ${weight} kg sont de ${totalAmount(dimensionSurchargeCAD, "cad")}.`;
    } else if (country === "japon") {
        return `Les frais de livraison pour un colis de ${weight} kg sont de ${totalAmount(dimensionSurchargeJPY, "jpy")}.`;
    } else {
        return "Ce pays n'est pas pris en charge.";
    }
};

console.log(shipmentPrices(2, 10, 150, 5, "japon"));

let shipmentTaxes = (declaredValue: number, country: ShipmentCountry): string => {
    if (country === "japon" && declaredValue > 5000) {
        return `Vos frais de douanes sont de ${declaredValue * 0.1} jpy`;
    } else if (country === "canada" && declaredValue > 20) {
        return `Vos frais de douanes sont de ${declaredValue * 0.15} cad`;
    } else {
        return "Il n'y a pas de frais de douanes si le colis est envoyé en France";
    }
};

console.log(shipmentTaxes(10, "japon"));
